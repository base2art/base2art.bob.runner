namespace Base2art.Bob.Runner.Features
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection.PortableExecutable;
    using FluentAssertions;
    using Procedures.NodeJs;
    using Procedures.NUnit;
    using Xunit;
    using Xunit.Abstractions;

    public class NunitFeatures
    {
        private readonly ITestOutputHelper helper;

        public NunitFeatures(ITestOutputHelper helper)
        {
            this.helper = helper;
        }

        [Fact]
        public async void ShouldRunNunit()
        {
            var nunit = new Base2art.Bob.Procedures.NUnit.NUnit3Test();
            var dir = new DirectoryInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "code"));

            this.helper.WriteLine(Environment.MachineName);
            Console.WriteLine(Environment.MachineName);

            var machineName = "S166-62-45-113";

            if (!dir.Exists && Environment.MachineName == machineName)
            {
                return;
            }

            List<DirectoryInfo> dirs = new List<DirectoryInfo>();

            foreach (var upD in dir.GetDirectories())
            {
                try
                {
                    foreach (var childDir in upD.GetDirectories())
                    {
                        if (childDir.Name.EndsWith("auth-service", StringComparison.OrdinalIgnoreCase))
                        {
                            dirs.Add(childDir);
                        }
                    }
                }
                catch (Exception)
                {
                }
            }

            var item = dirs.FirstOrDefault();
            if (item == null && Environment.MachineName == machineName)
            {
                return;
            }

            Environment.CurrentDirectory = item.FullName;

            var path = nunit.FindTargets(null);

            path.Length.Should().Be(1);
            path[0].Name.Should().Contain("Features");

            var result = await nunit.RunAsync(new NUnit3TestData(), new ProcedureParameters(Console.Out, Console.Error));
            result.Should().Be(0);
        }

//
//        public string GetOSVersion()
//        {
//            switch (Environment.OSVersion.Platform)
//            {
//                case PlatformID.Win32S:
//                    return "Win 3.1";
//                case PlatformID.Win32Windows:
//                    switch (Environment.OSVersion.Version.Minor)
//                    {
//                        case 0:
//                            return "Win95";
//                        case 10:
//                            return "Win98";
//                        case 90:
//                            return "WinME";
//                    }
//
//                    break;
//
//                case PlatformID.Win32NT:
//                    switch (Environment.OSVersion.Version.Major)
//                    {
//                        case 3:
//                            return "NT 3.51";
//                        case 4:
//                            return "NT 4.0";
//                        case 5:
//                            switch (Environment.OSVersion.Version.Minor)
//                            {
//                                case 0:
//                                    return "Win2000";
//                                case 1:
//                                    return "WinXP";
//                                case 2:
//                                    return "Win2003";
//                            }
//
//                            break;
//
//                        case 6:
//                            switch (Environment.OSVersion.Version.Minor)
//                            {
//                                case 0:
//                                    return "Vista/Win2008Server";
//                                case 1:
//                                    return "Win7/Win2008Server R2";
//                                case 2:
//                                    return "Win8/Win2012Server";
//                                case 3:
//                                    return "Win8.1/Win2012Server R2";
//                            }
//
//                            break;
//                        case 10: //this will only show up if the application has a manifest file allowing W10, otherwise a 6.2 version will be used
//                            return "Windows 10";
//                    }
//
//                    break;
//
//                case PlatformID.WinCE:
//                    return "Win CE";
//            }
//
//            return "Unknown";
//        }
    }
}