namespace Base2art.Bob.Runner.Features
{
    using System.IO;
    using System.Text;
    using System.Xml.Linq;
    using FluentAssertions;
    using Procedures.MsBuild;
    using Xunit;
    using Xunit.Abstractions;

    public class NuGetFeature
    {
        private readonly TextWriter outputHelper;

        public NuGetFeature(ITestOutputHelper outputHelper)
        {
            this.outputHelper = new WrappedTextWriter(outputHelper);
        }

        [Theory]
        [InlineData("simpleKey1", "https://nuget.base2art.com", 0, "simpleKey1", "https://nuget.base2art.com", 2)]
        [InlineData("simpleKey1", "https://nuget.base2art.com", 2, "simpleKey1", "https://nuget.base2art.com", 2)]
        [InlineData("simpleKey1", "https://nuget.base2art.com", 3, "simpleKey1", "https://nuget.base2art.com", 3)]

        //
        [InlineData("", "https://nuget.base2art.com", 0, "3834f690c23911bfe50a2acb168141c4", "https://nuget.base2art.com", 2)]
        [InlineData("", "https://nuget.base2art.com", 2, "3834f690c23911bfe50a2acb168141c4", "https://nuget.base2art.com", 2)]
        [InlineData("", "https://nuget.base2art.com", 3, "3834f690c23911bfe50a2acb168141c4", "https://nuget.base2art.com", 3)]
        public async void ShouldRun(string key, string value, int protocol, string expectedKey, string expectedUrl, int expectedProtocolVersion)
        {
            string inputText = @"<?xml version='1.0' encoding='utf-8'?>
<configuration>
  <packageSources>
    <add key='nuget.org' value='https://api.nuget.org/v3/index.json' protocolVersion='3' />
    <add key='LocalHostW' value='W:\data\nuget-repository' />
    <add key='LocalHostY' value='Y:\data\nuget-repository' />
  </packageSources>
  <disabledPackageSources>
  </disabledPackageSources>
</configuration>";

            string outputText = $@"<configuration>
  <packageSources>
    <add key=""nuget.org"" value=""https://api.nuget.org/v3/index.json"" protocolVersion=""3"" />
    <add key=""LocalHostW"" value=""W:\data\nuget-repository"" />
    <add key=""LocalHostY"" value=""Y:\data\nuget-repository"" />
    <add key=""{expectedKey}"" value=""{expectedUrl}"" protocolVersion=""{expectedProtocolVersion}"" />
  </packageSources>
  <disabledPackageSources></disabledPackageSources>
</configuration>";

            var parms = new ProcedureParameters(this.outputHelper, this.outputHelper);

            var nugetSourceAddData = new NugetSourceAddData();
            nugetSourceAddData.Key = key;
            nugetSourceAddData.Url = value;
            nugetSourceAddData.ProtocolVersion = protocol;

            var sources1 = new CustomNugetSourceAdd();
            sources1.InputText = inputText;
            await sources1.RunAsync(nugetSourceAddData, parms);
            sources1.OutputText.Should().Be(outputText);

            var sources2 = new CustomNugetSourceAdd();
            sources2.InputText = sources1.OutputText;
            await sources2.RunAsync(nugetSourceAddData, parms);
            sources2.OutputText.Should().Be("");
        }
    }

    public class CustomNugetSourceAdd : NugetSourceAdd
    {
        public string InputText = @"";
        public string OutputText = @"";
        public XDocument Output;

        protected override XDocument ReadXml(string nugetPath)
        {
            return XDocument.Parse(this.InputText);
        }

        protected override void WriteXml(string nugetPath, XDocument xElement)
        {
            this.OutputText = xElement.ToString();
            this.Output = xElement;
        }
    }

    public class WrappedTextWriter : TextWriter
    {
        private readonly ITestOutputHelper outputHelper;

        public WrappedTextWriter(ITestOutputHelper outputHelper)
        {
            this.outputHelper = outputHelper;
        }

        public override Encoding Encoding { get; }

        public override void WriteLine()
        {
            this.outputHelper.WriteLine(string.Empty);
        }

        public override void WriteLine(string value)
        {
            this.outputHelper.WriteLine(value);
        }

        public override void Write(char value)
        {
            this.outputHelper.WriteLine(value.ToString());
        }
    }
}