namespace Base2art.Bob.Runner.Features
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using FluentAssertions;
    using Procedures.NodeJs;
    using Xunit;
    using Xunit.Abstractions;

    public class UnitTest1
    {
        public UnitTest1(ITestOutputHelper helper) => this.helper = helper;

        private readonly ITestOutputHelper helper;

        [Theory]
        [InlineData("{\"id\":\"base2art:item:0.0.0.2\"}")]
        [InlineData("{\"org\":\"base2art\",\"name\":\"item\",\"version\":\"0.0.0.2\"}")]
        [InlineData("{'id':'base2art:item:0.0.0.2'}")]
        [InlineData("{'org':'base2art','name':'item','version':'0.0.0.2'}")]
        [InlineData("{id:'base2art:item:0.0.0.2'}")]
        [InlineData("{org:'base2art',name:'item',version:'0.0.0.2'}")]
        public void Test1(string block)
        {
            var proc = new ProcedureParser(new InternalSerializer());
            var item = proc.Parse("UNIT-TEST", block);
            item.Length.Should().Be(1);

            item[0].Org.Should().Be("base2art");
            item[0].Name.Should().Be("item");
            item[0].Version.Should().Be("0.0.0.2");
        }

        [Theory]
        [InlineData("Script0.ps1", 64)]
        [InlineData("Script1.ps1", 0)]
        [InlineData("Script2.ps1", 0)]
        [InlineData("Script3.ps1", 42)]
        [InlineData("Script4.ps1", 2)]
        public async void ShouldParsePowerShell(string path, int expectedExitCode)
        {
            var tempPath = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), "2C8D8EA1D")).FullName;

            File.WriteAllText(Path.Combine(tempPath, "Script1.ps1"), "ls");
            File.WriteAllText(Path.Combine(tempPath, "Script2.ps1"), "pwd");
            File.WriteAllText(Path.Combine(tempPath, "Script3.ps1"), "exit 42");
            File.WriteAllText(Path.Combine(tempPath, "Script4.ps1"), "exit 2");

            var content = $@"
{{
    ""id"": ""base2art:powershell-6:0.0.0.1"",
            ""data"":  {{  ""path"": ""{path}"" }}
}}
    ";

            Environment.CurrentDirectory = tempPath;
            var internalSerializer = new InternalSerializer();
            var proc = new ProcedureParser(internalSerializer);
            var data = proc.Parse("UNIT-TEST", content)[0];
            var runner = new ProcedureRunner(
                                             new CustomTextWriter(this.helper),
                                             new CustomTextWriter(this.helper),
                                             new ProcedureFinder(),
                                             internalSerializer);
            var result = await runner.RunAsync(data, false);
            result.Should().Be(expectedExitCode);
        }

        private class CustomTextWriter : TextWriter
        {
            private readonly ITestOutputHelper output;

            public CustomTextWriter(ITestOutputHelper output) => this.output = output;

            public override Encoding Encoding { get; }

            public override void Write(string value)
            {
                this.output.WriteLine(value);
            }

            public override void WriteLine(string value)
            {
                this.output.WriteLine(value);
            }
        }

        [Fact]
        public void ShouldParseList()
        {
            var internalSerializer = new InternalSerializer();

            var list = @"[""--modules-folder"", ""./build/node_modules"", ""--production=true"", ""--non-interactive""]";
            var result = (List<string>) internalSerializer.Deserialize(list, typeof(List<string>));
            result.Count.Should().Be(4);
        }

        [Fact]
        public async void ShouldParsePowerShellDirDiff()
        {
            var tempPathParent = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), "2C8D8EA1E")).FullName;
            var tempPathChild = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), "2C8D8EA1E", "child")).FullName;

            File.WriteAllText(Path.Combine(tempPathParent, "Script1.ps1"), "ls");
            File.WriteAllText(Path.Combine(tempPathChild, "Script1.ps1"), "ls");

            var internalSerializer = new InternalSerializer();
            var proc = new ProcedureParser(internalSerializer);

            var content1 = @"
{
    ""id"": ""base2art:powershell-6:0.0.0.1"",
            ""data"":  {  ""path"": ""Script1.ps1"" }
}
    ";

            Environment.CurrentDirectory = tempPathParent;

            var output1 = new StringWriter();
            var data1 = proc.Parse("UNIT-TEST", content1)[0];
            var runner1 = new ProcedureRunner(
                                              output1,
                                              new CustomTextWriter(this.helper),
                                              new ProcedureFinder(),
                                              internalSerializer);
            var result1 = await runner1.RunAsync(data1, false);

            ////
            var content2 = @"
{
    ""id"": ""base2art:powershell-6:0.0.0.1"",
            ""data"":  {  ""path"": ""child/Script1.ps1"" }
}
    ";

            Environment.CurrentDirectory = tempPathParent;
            var output2 = new StringWriter();
            var data2 = proc.Parse("UNIT-TEST", content2)[0];
            var runner2 = new ProcedureRunner(
                                              output2,
                                              new CustomTextWriter(this.helper),
                                              new ProcedureFinder(),
                                              internalSerializer);
            var result2 = await runner2.RunAsync(data2, false);

            output1.GetStringBuilder().ToString().Should().Be(output2.GetStringBuilder().ToString());
            result2.Should().Be(0);
        }

        [Fact]
        public void ShouldParseYarn()
        {
            var content = @"
{
    ""id"": ""base2art:yarn-command:0.0.0.1"",
            ""data"":  { 

                ""command"": ""install"", 
                ""targetPath"": ""package.json"", 
                ""parameters"": [""--modules-folder"", ""./build/node_modules"", ""--production=true"", ""--non-interactive""] 
            }
        }
";

            var internalSerializer = new InternalSerializer();
            var proc = new ProcedureParser(internalSerializer);
            var data = proc.Parse("UNIT-TEST", content)[0];
            var item = ProcedureRunner.GetMethodAndData(typeof(YarnCommand), data, internalSerializer);

            var cmdData = (YarnCommandData) item.Item3;

            cmdData.Should().NotBeNull();

            cmdData.Parameters.Should().HaveCount(4);
            cmdData.Parameters[0].Should().Be("--modules-folder");
        }
        
        [Fact]
        public void ShouldParseYarnListed()
        {
            var content = @"
[
  {
    ""id"": ""base2art:yarn-command:0.0.0.1"",
    ""data"":  { 
                ""command"": ""install"", 
                ""targetPath"": ""package.json"", 
                ""parameters"": [""--modules-folder"", ""./build/node_modules"", ""--production=true"", ""--non-interactive""] 
    }
  }
]
";

            var internalSerializer = new InternalSerializer();
            var proc = new ProcedureParser(internalSerializer);
            var data = proc.Parse("UNIT-TEST", content)[0];
            var item = ProcedureRunner.GetMethodAndData(typeof(YarnCommand), data, internalSerializer);

            var cmdData = (YarnCommandData) item.Item3;

            cmdData.Should().NotBeNull();

            cmdData.Parameters.Should().HaveCount(4);
            cmdData.Parameters[0].Should().Be("--modules-folder");
        }

        [Fact]
        public void ShouldResolveUrl()
        {
            var result = Procedures.Procedures.FileOrValue<YarnCommand>(null, "https://www.nuget.org/api/v2/package/Base2art.Standard.DataStorage.SqlClassMapper.App/0.0.0.1", skipDownload:true);
            result.Should().Be("https://www.nuget.org/api/v2/package/Base2art.Standard.DataStorage.SqlClassMapper.App/0.0.0.1");

        }
    }
}