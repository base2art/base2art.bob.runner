namespace Base2art.Bob.Runner.Features
{
    using System.Linq;
    using FluentAssertions;
    using Xunit;

    public class PhaseParsingFeature
    {
        [Theory]
        [InlineData("999", "-whatif", 0, 999, true)]
        [InlineData("999", "", 0, 999, false)]
        [InlineData("site", "-whatif", 0, 299, true)]
        [InlineData("default", "", 0, 199, false)]
        [InlineData("clean", "", 0, 99, false)]
        [InlineData("114", "", 0, 114, false)]
        [InlineData("100:114", "", 100, 114, false)]
        [InlineData(":114", "", 0, 114, false)]
        [InlineData("104:", "", 104, 999, false)]
        public void ShouldParse(string phase, string flags, int expectedMin, int expectedMax, bool expectedWhatif)
        {
            var options = ConsoleParametersParser.Parse(new[] {phase, flags}.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray());
            options.MinPhase.Should().Be(expectedMin);
            options.MaxPhase.Should().Be(expectedMax);
            options.WhatIf.Should().Be(expectedWhatif);
            options.Target.Should().Be("--");
        }

        [Theory]
        [InlineData(new[] {"999", "-whatif"}, 0, 999, true, "--")]
        [InlineData(new[] {"-whatif", "999"}, 0, 999, true, "--")]
        [InlineData(new[] {"-whatif", "999", "--target=any"}, 0, 999, true, "any")]
        public void ShouldParseFullLine(string[] args, int expectedMin, int expectedMax, bool expectedWhatif, string expectedTarget)
        {
            var options = ConsoleParametersParser.Parse(args.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray());
            options.MinPhase.Should().Be(expectedMin);
            options.MaxPhase.Should().Be(expectedMax);
            options.WhatIf.Should().Be(expectedWhatif);
            options.Target.Should().Be(expectedTarget);
        }
    }
}