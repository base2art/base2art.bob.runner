﻿namespace Base2art.Bob.Procedures.PowerShell
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Diagnostic;

    [Procedure("base2art", "powershell-6", "0.0.0.1")]
    public class PowerShell6Runner : IProcedure<PowerShell6RunnerData>
    {
        public async Task<int> RunAsync(PowerShell6RunnerData item, ProcedureParameters parameters)
        {
            var factory = new ProcessBuilderFactory();
            var fileInfo = await Environments.FindFileInPath("pwsh", "pwsh.exe");

            var workingDir = Environment.CurrentDirectory;
            
            if (!string.IsNullOrWhiteSpace(item.WorkingDirectory))
            {
                workingDir = Path.Combine(Environment.CurrentDirectory, item.WorkingDirectory);
            }

            var returnCode = await factory.Create()
                                          .WithExecutable(fileInfo.FullName)
                                          .WithArguments(new[] {"-File", item.Path})
                                          .WithWorkingDirectory(workingDir)
                                          .WithOutputWriter(parameters.OutputWriter)
                                          .WithErrorWriter(parameters.ErrorWriter)
                                          .Execute();
            return returnCode;
        }
    }
}