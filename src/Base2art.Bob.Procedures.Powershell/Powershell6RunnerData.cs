﻿namespace Base2art.Bob.Procedures.PowerShell
{
    public class PowerShell6RunnerData
    {
        public string Path { get; set; }
        
        public string WorkingDirectory { get; set; }
    }
}