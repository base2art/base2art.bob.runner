﻿namespace Base2art.Bob.Procedures
{
    using System;
    using System.IO;
    using System.Net;
    using System.Threading.Tasks;
    using Diagnostic;

    public static class Procedures
    {
        public static Task<int> Execute(this IProcessBuilderFactory factory, ProcedureParameters parameters, string exePath, params string[] parms)
            => factory.Create()
                      .WithExecutable(exePath)
                      .WithArguments(parms)
                      .WithOutputWriter(parameters.OutputWriter)
                      .WithErrorWriter(parameters.ErrorWriter)
                      .Execute();

        public static string FileOrValue<T>(this IProcedure<T> proc, string fileOrValue, bool skipDownload = false)
        {
            bool IsUriLocal(string value, out Uri uriValue)
            {
                bool result = Uri.TryCreate(value, UriKind.Absolute, out var uriResult)
                              && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                uriValue = uriResult;
                return result;
            }

            if (string.IsNullOrWhiteSpace(fileOrValue))
            {
                return null;
            }

            if (File.Exists(fileOrValue))
            {
                return File.ReadAllText(fileOrValue);
            }

            if (!IsUriLocal(fileOrValue, out var uri))
            {
                return fileOrValue;
            }

            if (!skipDownload)
            {
                using (var wc = new WebClient())
                {
                    fileOrValue = wc.DownloadString(uri);
                }
            }

            return fileOrValue;
        }

        public static Task<string> ValueOrAsync<T>(this IProcedure<T> procedure, string value, Func<Task<string>> backing)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                return Task.FromResult(value);
            }

            return backing();
        }

        public static string ValueOr<T>(this IProcedure<T> procedure, string value, Func<string> backing)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                return value;
            }

            return backing();
        }

        public static FileInfo[] FilesByExtension(DirectoryInfo di, string filter) => di.GetFiles(filter, SearchOption.AllDirectories);

        public static FileInfo[] TargetFiles<T>(
            this IProcedure<T> procedure,
            string targetPath,
            string searchFilter)
        {
            return procedure.TargetFiles(targetPath, x => FilesByExtension(x, searchFilter));
        }

        public static FileInfo[] TargetFiles<T>(this IProcedure<T> procedure, string targetPath,
                                                Func<DirectoryInfo, FileInfo[]> searchFilter)
        {
            if (string.IsNullOrWhiteSpace(targetPath))
            {
                var di = new DirectoryInfo(Environment.CurrentDirectory);
                return searchFilter(di);
            }

            return new[] {new FileInfo(targetPath)};
        }

        public static DirectoryInfo[] TargetDirectories<T>(
            this IProcedure<T> procedure, string targetPath,
            Func<DirectoryInfo, DirectoryInfo[]> search)
        {
            if (string.IsNullOrWhiteSpace(targetPath))
            {
                var di = new DirectoryInfo(Environment.CurrentDirectory);
                return search(di);
            }

            return new[] {new DirectoryInfo(targetPath)};
        }

        public static string[] Targets<T>(this IProcedure<T> procedure, string targetPath, string filter)
            => string.IsNullOrWhiteSpace(targetPath)
                   ? Directory.GetFiles(Environment.CurrentDirectory, filter, SearchOption.AllDirectories)
                   : new[] {targetPath};
    }
}