﻿namespace Base2art.Bob.Procedures.ProgramExecution
{
    using System;
    using System.Threading.Tasks;
    using Diagnostic;

    [Procedure("base2art", "run-program", "0.0.0.1")]
    public class ProgramRunner : IProcedure<ProgramRunnerData>
    {
        public async Task<int> RunAsync(ProgramRunnerData item, ProcedureParameters parameters)
        {
            var factory = new ProcessBuilderFactory();

            var workingDir = Environment.CurrentDirectory;

            var builder = factory.Create()
                                 .WithExecutable(item.Executable)
                                 .WithWorkingDirectory(workingDir)
                                 .WithOutputWriter(parameters.OutputWriter)
                                 .WithErrorWriter(parameters.ErrorWriter);

            if (item?.Parameters?.Length > 0)
            {
                builder = builder.WithArguments(item.Parameters);
            }

            var returnCode = await builder.Execute();
            return returnCode;
        }
    }
}