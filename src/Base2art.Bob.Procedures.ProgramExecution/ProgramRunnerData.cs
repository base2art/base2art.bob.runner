﻿namespace Base2art.Bob.Procedures.ProgramExecution
{
    public class ProgramRunnerData
    {
        public string Executable { get; set; }
        public string[] Parameters { get; set; }
    }
}