﻿namespace Base2art.Bob.Agent
{
    using System;
    using System.Threading.Tasks;

    public interface IProcedureFinder
    {
        Task<Type> Find(string org, string name, string version);
    }
}