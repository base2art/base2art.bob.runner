namespace Base2art.Bob.Agent
{
    using System;

    public interface ISerializer
    {
        string Serialize(object data);
        T Deserialize<T>(string content);
        object Deserialize(string content, Type type);
    }
}