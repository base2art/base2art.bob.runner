﻿namespace Base2art.Bob.Procedures.Security
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using Serialization;

    [Procedure("base2art", "encrypt-file-json-dict", "0.0.0.1")]
    public class FileDictionaryEncryptor : IProcedure<FileDictionaryEncryptorData>
    {
        private static readonly CamelCasingSimpleJsonSerializer CamelCasingSimpleJsonSerializer = new CamelCasingSimpleJsonSerializer();

        public Task<int> RunAsync(FileDictionaryEncryptorData item, ProcedureParameters defaultParameters)
        {
            var source = item.Source;
            if (!File.Exists(source))
            {
                return Task.FromResult(0);
            }

            var encrypted = item.OutputFile ?? source + ".encrypted";
            if (File.Exists(encrypted))
            {
                return Task.FromResult<int>(0);
            }

            var secret = this.FileOrValue(item.Secret)?.Trim() ?? throw new ArgumentNullException(nameof(item.Secret));
            var salt = this.FileOrValue(item.Salt)?.Trim() ?? throw new ArgumentNullException(nameof(item.Salt));

            var saltyBytes = Encoding.Default.GetBytes(salt);

            var content = File.ReadAllText(source);
            var dict = CamelCasingSimpleJsonSerializer.Deserialize<Dictionary<string, string>>(content);

            var output = new Dictionary<string, string>();

            foreach (var dictItem in dict)
            {
                output[dictItem.Key] = Cryptography.EncryptStringAES(dictItem.Value, secret, saltyBytes);
            }

            File.WriteAllText(encrypted, CamelCasingSimpleJsonSerializer.Serialize(output));
            return Task.FromResult<int>(0);
        }
    }
}