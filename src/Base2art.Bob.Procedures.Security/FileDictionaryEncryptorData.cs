﻿namespace Base2art.Bob.Procedures.Security
{
    public class FileDictionaryEncryptorData
    {
        public string Source { get; set; }
        public string Salt { get; set; }
        public string Secret { get; set; }
        public string OutputFile { get; set; }
    }
}