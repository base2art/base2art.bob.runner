namespace Base2art.Bob.Procedures.Security
{
    using System;
    using System.IO;
    using System.Security.Cryptography;

    public static class Cryptography
    {
        public static string DecryptStringAES(string cipherText, string sharedSecret, byte[] salt)
        {
            if (string.IsNullOrEmpty(cipherText))
            {
                throw new ArgumentNullException(nameof(cipherText));
            }

            if (string.IsNullOrEmpty(sharedSecret))
            {
                throw new ArgumentNullException(nameof(sharedSecret));
            }

            RijndaelManaged rijndaelManaged = null;
            try
            {
                var rfc2898DeriveBytes = new Rfc2898DeriveBytes(sharedSecret, salt);
                var buffer = Convert.FromBase64String(cipherText);
                using (var memoryStream = new MemoryStream(buffer))
                {
                    rijndaelManaged = new RijndaelManaged();
                    rijndaelManaged.Key = rfc2898DeriveBytes.GetBytes(rijndaelManaged.KeySize / 8);
                    rijndaelManaged.IV = ReadByteArray(memoryStream);
                    var transform = rijndaelManaged.CreateDecryptor(rijndaelManaged.Key, rijndaelManaged.IV);
                    using (var stream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Read))
                    {
                        using (var streamReader = new StreamReader(stream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
            finally
            {
                rijndaelManaged?.Clear();
            }
        }

        public static string EncryptStringAES(string plainText, string sharedSecret, byte[] salt)
        {
            if (string.IsNullOrEmpty(plainText))
            {
                throw new ArgumentNullException(nameof(plainText));
            }

            if (string.IsNullOrEmpty(sharedSecret))
            {
                throw new ArgumentNullException(nameof(sharedSecret));
            }

            RijndaelManaged rijndaelManaged = null;
            try
            {
                var rfc2898DeriveBytes = new Rfc2898DeriveBytes(sharedSecret, salt);
                rijndaelManaged = new RijndaelManaged();
                rijndaelManaged.Key = rfc2898DeriveBytes.GetBytes(rijndaelManaged.KeySize / 8);
                var transform = rijndaelManaged.CreateEncryptor(rijndaelManaged.Key, rijndaelManaged.IV);
                using (var memoryStream = new MemoryStream())
                {
                    memoryStream.Write(BitConverter.GetBytes(rijndaelManaged.IV.Length), 0, 4);
                    memoryStream.Write(rijndaelManaged.IV, 0, rijndaelManaged.IV.Length);
                    using (var stream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write))
                    {
                        using (var streamWriter = new StreamWriter(stream))
                        {
                            streamWriter.Write(plainText);
                        }
                    }

                    return Convert.ToBase64String(memoryStream.ToArray());
                }
            }
            finally
            {
                rijndaelManaged?.Clear();
            }
        }

        private static byte[] ReadByteArray(Stream s)
        {
            var array = new byte[4];
            if (s.Read(array, 0, array.Length) != array.Length)
            {
                throw new SystemException("Stream did not contain properly formatted byte array");
            }

            var array2 = new byte[BitConverter.ToInt32(array, 0)];
            if (s.Read(array2, 0, array2.Length) != array2.Length)
            {
                throw new SystemException("Did not read byte array properly");
            }

            return array2;
        }
    }
}