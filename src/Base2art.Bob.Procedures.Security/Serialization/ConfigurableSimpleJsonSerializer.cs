namespace Base2art.Bob.Procedures.Security.Serialization
{
    using System;
    using Converters;
    using Internals;

    /// <summary>
    ///     A simple serializer that allows the user to configure how the output is serialised.
    /// </summary>
    internal sealed class ConfigurableSimpleJsonSerializer : IJsonSerializer
    {
        private readonly bool camelCaseOutput;
        private readonly Func<IJsonSerializer, IConverter[]> converters;

        /// <summary>
        ///     Creates a new instance of the <see cref="ConfigurableSimpleJsonSerializer" /> class.
        /// </summary>
        /// <param name="camelCaseOutput">A value indicating that the output should be camelCased.</param>
        /// <param name="converters">A custom set of converters that can augment how data is serialized.</param>
        public ConfigurableSimpleJsonSerializer(bool camelCaseOutput, Func<IJsonSerializer, IConverter[]> converters)
        {
            this.camelCaseOutput = camelCaseOutput;
            this.converters = converters;
        }

        /// <summary>
        ///     Serialize an object to a string.
        /// </summary>
        /// <param name="item">The item to serialize.</param>
        /// <typeparam name="T">The type of object to serialize.</typeparam>
        /// <returns>The serialized value.</returns>
        public string Serialize<T>(T item) => SimpleJson.SerializeObject(item, this.CreateSerializerStrategy());

        /// <summary>
        ///     Deserialze a string to an object.
        /// </summary>
        /// <param name="text">The text to deserialize.</param>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <returns>The deserialized object.</returns>
        public T Deserialize<T>(string text) => SimpleJson.DeserializeObject<T>(text, this.CreateSerializerStrategy());

        /// <summary>
        ///     Deserialze a string to an object.
        /// </summary>
        /// <param name="text">The text to deserialize.</param>
        /// <param name="type">The type of object to return.</param>
        /// <returns>The deserialized object.</returns>
        public object Deserialize(string text, Type type) => SimpleJson.DeserializeObject(text, type, this.CreateSerializerStrategy());

        /// <summary>
        ///     A method to map property names to output names/
        /// </summary>
        /// <param name="name">The name to map.</param>
        /// <returns>The value.</returns>
        public string MapName(string name) => this.CreateSerializerStrategy().MapName(name);

        public object DeserializePrimitiveValue(string value, Type valueType)
        {
            var strategy = this.CreateSerializerStrategy();
            return strategy.DeserializeObject(value, valueType);
        }

        public string SerializePrimitiveValue(object value) => SimpleJson.SerializeObject(value, this.CreateSerializerStrategy(), true);

        private CustomPocoJsonSerializerStrategy CreateSerializerStrategy() =>
            new CustomPocoJsonSerializerStrategy(this.converters(this), this.camelCaseOutput);
    }
}

