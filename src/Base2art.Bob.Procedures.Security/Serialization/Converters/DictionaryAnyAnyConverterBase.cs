namespace Base2art.Bob.Procedures.Security.Serialization.Converters
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Internals;

    /// <summary>
    ///     A Dictionary deserializer.
    /// </summary>
    internal abstract class DictionaryAnyAnyConverterBase : IConverter
    {
        public DictionaryAnyAnyConverterBase(IJsonSerializer serializer) => this.Serializer = serializer;

        protected IJsonSerializer Serializer { get; }

        public bool CanDeserialize(Type type)
        {
            if (!(type.GetTypeInfo().IsGenericType && IsDictionary(type)))
            {
                return false;
            }

            var parameters = type.GetTypeInfo().GenericTypeArguments;
            var first = parameters[0];
            return this.IsMatch(first);
        }

        public object Deserialize(Type type, object value)
        {
            var instance = (IDictionary) Activator.CreateInstance(type);

            if (value == null)
            {
                return instance;
            }

            var obj = value as JsonObject;

            var parameters = type.GetTypeInfo().GenericTypeArguments;
            var first = parameters[0];
            var second = parameters[1];

            foreach (var key in obj.Keys)
            {
                var kVal = obj[key];

                var ser = this.Serializer.Serialize(kVal);

                if (ser.StartsWith("[") && second == typeof(object))
                {
                    second = typeof(object[]);
                }

                instance[this.DeserializeKey(first, key)] = this.Serializer.Deserialize(ser, second);
                //this.Serializer.Deserialize((string) kVal, second);
            }

            return instance;
        }

        public bool CanSerialize(Type objectType, object value)
        {
            if (!(objectType.GetTypeInfo().IsGenericType && IsDictionary(objectType)))
            {
                return false;
            }

            var parameters = objectType.GetTypeInfo().GenericTypeArguments;
            var first = parameters[0];

            return this.IsMatch(first);
        }

        public object Serialize(Type objectType, object value)
        {
            if (!(objectType.GetTypeInfo().IsGenericType && IsDictionary(objectType)))
            {
                throw new InvalidDataException("Serialization Mathcing Doesn't look correct");
            }

            var parameters = objectType.GetTypeInfo().GenericTypeArguments;
            var first = parameters[0];
            var second = parameters[1];

            var obj = new JsonObject();

            var hashTable = (IDictionary) value;
            foreach (var key in hashTable.Keys)
            {
                var keyOf = this.SerializeKey(objectType, key);

                var valueRegular = hashTable[key];
//                var valueOf = this.Serializer.Serialize(valueRegular);

                obj.Add(keyOf, valueRegular);
            }

            return obj;
        }

        protected abstract bool IsMatch(Type first);

        protected abstract string SerializeKey(Type objectType, object value);

        protected abstract object DeserializeKey(Type objectType, string key);

        private static bool IsDictionary(Type objectType)
        {
            var implementedInterfaces = objectType.GetGenericTypeDefinition().GetTypeInfo().ImplementedInterfaces;
            var dictType = typeof(IDictionary<,>);
            var contains = implementedInterfaces.Any(x => x.GetTypeInfo().IsGenericType && x.GetGenericTypeDefinition() == dictType);
            return contains;
        }
    }
}

