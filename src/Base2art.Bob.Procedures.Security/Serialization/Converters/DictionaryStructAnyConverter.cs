namespace Base2art.Bob.Procedures.Security.Serialization.Converters
{
    using System;
    using System.Reflection;

    /// <summary>
    ///     A Dictionary deserializer.
    /// </summary>
    internal class DictionaryStructAnyConverter : DictionaryAnyAnyConverterBase
    {
        public DictionaryStructAnyConverter(IJsonSerializer serializer) : base(serializer)
        {
        }

        protected override bool IsMatch(Type first) => first.GetTypeInfo().IsValueType;

        protected override string SerializeKey(Type objectType, object value)
            => this.Serializer.SerializePrimitiveValue(value);

        protected override object DeserializeKey(Type objectType, string key) => this.Serializer.DeserializePrimitiveValue(key, objectType);
    }
}

