namespace Base2art.Bob
{
    using System;

    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class ProcedureAttribute : Attribute
    {
        public ProcedureAttribute(string org, string name, string version)
        {
            this.Org = org;
            this.Name = name;
            this.Version = version;
        }

        public string Org { get; }
        public string Name { get; }
        public string Version { get; }
    }
}