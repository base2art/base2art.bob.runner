﻿namespace Base2art.Bob
{
    using System;
    using System.IO;

    public class ProcedureParameters
    {
        public ProcedureParameters(TextWriter outputWriter, TextWriter errorWriter)
        {
            this.OutputWriter = outputWriter ?? throw new ArgumentNullException(nameof(outputWriter));
            this.ErrorWriter = errorWriter ?? throw new ArgumentNullException(nameof(errorWriter));
        }

        public TextWriter OutputWriter { get; }
        public TextWriter ErrorWriter { get; }
    }
}