﻿namespace Base2art.Bob
{
    using System.Threading.Tasks;

    public interface IProcedure<in T>
    {
        Task<int> RunAsync(T item, ProcedureParameters defaultParameters);
    }
}