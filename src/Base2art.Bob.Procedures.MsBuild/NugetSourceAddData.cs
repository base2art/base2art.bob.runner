namespace Base2art.Bob.Procedures.MsBuild
{
    public class NugetSourceAddData
    {
        public string Key { get; set; }
        public string Url { get; set; }
        public int ProtocolVersion { get; set; }
        public string NugetConfigPath { get; set; }
    }
}