namespace Base2art.Bob.Procedures.MsBuild
{
    [Procedure("base2art", "msbuild-build", "0.0.0.1")]
    public class MsBuildBuild : MsBuildBase<MsBuildBuildData>
    {
        protected override string ActionTarget() => "Build";
    }
}