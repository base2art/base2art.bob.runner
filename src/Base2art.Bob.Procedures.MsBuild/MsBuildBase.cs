namespace Base2art.Bob.Procedures.MsBuild
{
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Diagnostic;
    using Microsoft.Win32;

    public abstract class MsBuildBase<T> : IProcedure<T>
        where T : MsBuildBaseData
    {
        public async Task<int> RunAsync(T item, ProcedureParameters defaultParameters)
        {
            var msBuildPath = this.ValueOr(item.MsBuildPath, this.MsBuildPath);
            var targets = this.Targets(item.TargetPath, "*.sln");
            var configuration = this.ValueOr(item.Configuration, this.Configuration);

            var processBuilderFactory = new ProcessBuilderFactory();

            foreach (var target in targets)
            {
                var result = await processBuilderFactory.Execute(
                                                                 defaultParameters,
                                                                 msBuildPath,
                                                                 target,
                                                                 $"/t:{this.ActionTarget()}",
                                                                 "/verbosity:quiet",
                                                                 "/nologo",
                                                                 $"/p:Configuration={configuration}");

                if (result != 0)
                {
                    return result;
                }
            }

            return 0;
        }

        protected abstract string ActionTarget();

        private string Configuration() => "Release";

        private string MsBuildPath()
        {
            var checks = new[]
                         {
                             @"C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin\msbuild.exe",
                             @"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\msbuild.exe"
                         };

            var f = checks.FirstOrDefault(File.Exists);
            if (f != null)
            {
                return f;
            }

            using (var key = Registry.LocalMachine.OpenSubKey(@"software\Microsoft\MSBuild\ToolsVersions\4.0"))
            {
                var msBuildToolsPath = (string) key.GetValue("MSBuildToolsPath");

                return Path.Combine(msBuildToolsPath, "msbuild.exe");
            }
        }
    }
}