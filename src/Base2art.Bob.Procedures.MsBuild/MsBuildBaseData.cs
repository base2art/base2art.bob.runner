namespace Base2art.Bob.Procedures.MsBuild
{
    public class MsBuildBaseData
    {
        public string MsBuildPath { get; set; }
        public string TargetPath { get; set; }
        public string Configuration { get; set; }
    }
}