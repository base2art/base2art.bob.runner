namespace Base2art.Bob.Procedures.MsBuild
{
    using System.IO;
    using System.Reflection;
    using System.Threading.Tasks;
    using Diagnostic;

    [Procedure("base2art", "nuget-restore", "0.0.0.1")]
    public class NugetRestore : IProcedure<NugetRestoreData>
    {
        public async Task<int> RunAsync(NugetRestoreData item, ProcedureParameters defaultParameters)
        {
            var nugetPath = this.NugetPath(item.NugetPath);
            var targets = this.Targets(item.TargetPath, "*.sln");

            var processBuilderFactory = new ProcessBuilderFactory();

            foreach (var target in targets)
            {
                var result = await processBuilderFactory.Execute(defaultParameters, nugetPath, "restore", target);

                if (result != 0)
                {
                    return result;
                }
            }

            return 0;
        }

        private string NugetPath(string nugetPath)
        {
            nugetPath = !string.IsNullOrWhiteSpace(nugetPath) ? nugetPath : Path.Combine(".deploy", "nuget.exe");
            if (!File.Exists(nugetPath))
            {
                var directoryName = Path.GetDirectoryName(nugetPath);
                if (directoryName != null)
                {
                    Directory.CreateDirectory(directoryName);
                }

                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Base2art.Bob.Procedures.MsBuild.Resources.nuget.exe"))
                {
                    using (var fh = File.OpenWrite(nugetPath))
                    {
                        stream.CopyTo(fh);
                    }
                }
            }

            return nugetPath;
        }
    }
}