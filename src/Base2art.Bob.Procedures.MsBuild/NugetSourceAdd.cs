namespace Base2art.Bob.Procedures.MsBuild
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using Diagnostic;

    [Procedure("base2art", "nuget-source-add", "0.0.0.1")]
    public class NugetSourceAdd : IProcedure<NugetSourceAddData>
    {
        public Task<int> RunAsync(NugetSourceAddData item, ProcedureParameters defaultParameters)
        {
            return Task.FromResult(this.RunSync(item, defaultParameters));
        }

        private int RunSync(NugetSourceAddData item, ProcedureParameters defaultParameters)
        {
            var nugetPath = this.NugetConfigPath(item.NugetConfigPath);

            if (string.IsNullOrWhiteSpace(nugetPath))
            {
                defaultParameters.ErrorWriter.WriteLine("File Not Found: NugetConfigPath");
                return -1;
            }

            string keyName = this.ValueOr(item.Key, () => this.Key(item));

            var xElement = this.ReadXml(nugetPath);

            var packageSources = xElement.XPathSelectElement($"/configuration/packageSources");

            if (packageSources.Elements("add").Any(x => x.Attribute("key").Value == keyName))
            {
                return 0;
            }

            packageSources.Add(new XElement("add",
                                            new XAttribute("key", keyName),
                                            new XAttribute("value", item.Url),
                                            new XAttribute("protocolVersion", item.ProtocolVersion == 0 ? 2 : item.ProtocolVersion)));
            this.WriteXml(nugetPath, xElement);

            return 0;
        }

        private string Key(NugetSourceAddData data)
        {
            return data.Url.HashAsGuid().ToString("N");
        }

        protected virtual void WriteXml(string nugetPath, XDocument xElement)
        {
            xElement.Save(nugetPath, SaveOptions.None);
        }

        protected virtual XDocument ReadXml(string nugetPath)
        {
            return XDocument.Load(nugetPath);
        }

        private string NugetConfigPath(string nugetPath)
        {
            if (!string.IsNullOrWhiteSpace(nugetPath))
            {
                return nugetPath;
            }

            var roots = new[]
                        {
                            Environment.SpecialFolder.ApplicationData,
                            Environment.SpecialFolder.UserProfile,
                            Environment.SpecialFolder.LocalApplicationData,
                        };
            

            var rootPaths = roots.SelectMany(x =>
            {
                var @base = Environment.GetFolderPath(x);
                return new[]
                       {
                           @base,
                           Path.Combine(@base, ".nuget"),
                       };
            });

            nugetPath = rootPaths.Select(x => Path.Combine(x, "NuGet", "NuGet.Config"))
                                 .FirstOrDefault(File.Exists);

            return nugetPath;
        }
    }
}