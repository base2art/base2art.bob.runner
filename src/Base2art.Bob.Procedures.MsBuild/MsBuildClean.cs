namespace Base2art.Bob.Procedures.MsBuild
{
    [Procedure("base2art", "msbuild-clean", "0.0.0.1")]
    public class MsBuildClean : MsBuildBase<MsBuildCleanData>
    {
        protected override string ActionTarget() => "Clean";
    }
}