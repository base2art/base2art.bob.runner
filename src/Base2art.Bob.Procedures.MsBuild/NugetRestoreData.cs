namespace Base2art.Bob.Procedures.MsBuild
{
    public class NugetRestoreData
    {
        public string TargetPath { get; set; }
        public string NugetPath { get; set; }
    }
}