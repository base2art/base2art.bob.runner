namespace Base2art.Bob.Procedures.NUnit
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using Diagnostic;

    [Procedure("base2art", "nunit3-test", "0.0.0.1")]
    public class NUnit3Test : IProcedure<NUnit3TestData>
    {
        public async Task<int> RunAsync(NUnit3TestData item, ProcedureParameters defaultParameters)
        {
            var targets = this.FindTargets(item.TargetPath);
            var installDir = this.ValueOr(item.ExecutableDirectory, this.ExecutableDirectory);

            if (targets.Length == 0)
            {
                return 0;
            }

            var asm = Assembly.GetExecutingAssembly();

            var prefix = "Base2art.Bob.Procedures.NUnit.Resources.nunit3.";
            var resxes = asm.GetManifestResourceNames().Where(x => x.StartsWith(prefix, StringComparison.OrdinalIgnoreCase));

            if (!Directory.Exists(installDir))
            {
                Directory.CreateDirectory(installDir);

                foreach (var resx in resxes)
                {
                    using (var ms = asm.GetManifestResourceStream(resx))
                    {
                        var nameFile = resx.Substring(prefix.Length);

                        var name = Path.Combine(installDir, nameFile);
                        File.WriteAllBytes(name, ms.ReadFully());
                    }
                }
            }

            var processBuilderFactory = new ProcessBuilderFactory();

            foreach (var target in targets)
            {
                var result = await processBuilderFactory.Create()
                                                        .WithOutputWriter(defaultParameters.OutputWriter)
                                                        .WithErrorWriter(defaultParameters.ErrorWriter)
                                                        .WithExecutable(Path.Combine(installDir, "nunit3-console.exe"))
                                                        .WithWorkingDirectory(Environment.CurrentDirectory)
                                                        .WithArguments(new string[] {target.FullName, "--noh", "--noresult"})
                                                        .Execute();

                if (result != 0)
                {
                    return result;
                }
            }

            return 0;
        }

        private string ExecutableDirectory()
        {
            return Path.Combine(".deploy", "nunit3");
        }

        protected virtual string ExtensionFilter() => "*.csproj";

        protected virtual bool IncludeTarget(FileInfo path)
        {
            try
            {
                return path.Exists &&
                       File.ReadAllText(path.FullName).ToUpperInvariant().Contains("NUnit".ToUpperInvariant());
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected virtual FileInfo GetPath(FileInfo path)
        {
            try
            {
                var doc = XDocument.Load(path.FullName);
                var element = doc.Root.Elements()
                                 .SelectMany(x => x.Elements())
                                 .FirstOrDefault(x => x.Name.LocalName == "AssemblyName");
                var name = element != null
                               ? element.Value
                               : path.Name.Substring(0, path.Name.Length - 7);

                var dll = path.Directory.GetFiles($"{name}.dll", SearchOption.AllDirectories).FirstOrDefault();

                return dll;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public FileInfo[] FindTargets(string path)
        {
            var targets = this.TargetFiles(path, this.ExtensionFilter());
            return targets.Where(this.IncludeTarget)
                          .Select(x => this.GetPath(x))
                          .Where(x => x != null)
                          .ToArray();
        }
    }
}