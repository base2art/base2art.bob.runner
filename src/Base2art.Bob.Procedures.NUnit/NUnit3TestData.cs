namespace Base2art.Bob.Procedures.NUnit
{
    public class NUnit3TestData
    {
        public string TargetPath { get; set; }
        public string ExecutableDirectory { get; set; }
    }
}