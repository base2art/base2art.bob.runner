﻿namespace Base2art.Bob
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Agent;
    using Serialization;

    public class ProcedureParser
    {
        private readonly ISerializer serializer;

        public ProcedureParser(ISerializer serializer) => this.serializer = serializer;

        public ProcedureData[] Parse(FileInfo fileInfo)
        {
            var fileInfoFullName = fileInfo.FullName;
            var content = File.ReadAllText(fileInfoFullName).Trim();

            return this.Parse(fileInfoFullName, content);
        }

        public ProcedureData[] Parse(string filePath, string content) => this.ParseInternal(filePath, content)
                                                                             .Select(NormalizeProcedureData)
                                                                             .ToArray();

        private static ProcedureData NormalizeProcedureData(ProcedureData data)
        {
            var newData = new ProcedureData
                          {
                              Data = data.Data,
                              Id = data.Id,
                              Org = data.Org,
                              Name = data.Name,
                              Version = data.Version
                          };

            if (!string.IsNullOrWhiteSpace(data.Id))
            {
                var parts = data.Id.Split(':', StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray();
                if (parts.Length == 3)
                {
                    newData.Org = parts[0];
                    newData.Name = parts[1];
                    newData.Version = parts[2];
                }
            }

            newData.Id = $"{newData.Org}:{newData.Name}:{newData.Version}";

            return newData;
        }

        private ProcedureData[] ParseInternal(string filePath, string content)
        {
            try
            {
                return this.ParseInternalUnSafe(content);
            }
            catch (Exception e)
            {
                Console.WriteLine(filePath);
                Console.WriteLine(content);
                Console.WriteLine(e);
                throw;
            }
        }

        private ProcedureData[] ParseInternalUnSafe(string content)
        {
            var first = content.FirstOrDefault(x => x == '{' || x == '[');

            switch (first)
            {
                case default(char):
                    return new ProcedureData[0];
                case '[':
                    return this.serializer.Deserialize<List<ProcedureData>>(content).ToArray();
                case '{':
                    return new[] {this.serializer.Deserialize<ProcedureData>(content)};
                default:
                    return this.serializer.Deserialize<List<ProcedureData>>(content).ToArray();
            }
        }
    }
}