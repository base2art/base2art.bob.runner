﻿namespace Base2art.Bob
{
    using System;
    using Agent;
    using Serialization;

    public class InternalSerializer : ISerializer
    {
        private readonly CamelCasingSimpleJsonSerializer ser;

        public InternalSerializer() => this.ser = new CamelCasingSimpleJsonSerializer();

        public string Serialize(object data) => this.ser.Serialize(data);

        public T Deserialize<T>(string content) => this.ser.Deserialize<T>(content);

        public object Deserialize(string content, Type type) => this.ser.Deserialize(content, type);
    }
}