﻿namespace Base2art.Bob
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Threading.Tasks;
    using Agent;

    public class ProcedureRunner
    {
        private readonly TextWriter errorWriter;
        private readonly IProcedureFinder finder;
        private readonly TextWriter outputWriter;
        private readonly ISerializer serializer;

        public ProcedureRunner(TextWriter outputWriter, TextWriter errorWriter, IProcedureFinder finder, ISerializer serializer)
        {
            this.outputWriter = outputWriter;
            this.errorWriter = errorWriter;
            this.finder = finder;
            this.serializer = serializer;
        }

        public async Task<int> RunAsync(ProcedureData data, bool whatIf)
        {
            var type = await this.finder.Find(data.Org, data.Name, data.Version);

            if (type == null)
            {
                this.outputWriter.WriteLine($"Error Finding... [`{data.Id}`]");
                return -3;
            }

            var (item, method, parameter) = GetMethodAndData(type, data, this.serializer);

            this.outputWriter.WriteLine($"Starting [`{data.Id}`]");

            try
            {
                if (!whatIf)
                {
                    // !additionalParm? (Task<int>) method.Invoke(item, new[] {parameter})
//                    : 
                    var runAsync = (Task<int>) method.Invoke(item, new[]
                                                                   {
                                                                       parameter,
                                                                       new ProcedureParameters(this.outputWriter, this.errorWriter)
                                                                   });
                    return await runAsync;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                this.errorWriter.WriteLine(e.ToString());
//                Console.WriteLine(e);
            }
            finally
            {
                this.outputWriter.WriteLine($"Completed [`{data.Id}`]");
            }

            return -2;
        }

        public static (object, MethodInfo, object) GetMethodAndData(Type type, ProcedureData data, ISerializer serializerValue)
        {
            bool hasInterface(Type inner, Type question)
            {
                foreach (var i in inner.GetInterfaces())
                {
                    if (i.IsGenericType && i.GetGenericTypeDefinition() == question)
                    {
                        return true;
                    }
                }

                return false;
            }

            if (hasInterface(type, typeof(IProcedure<>)))
            {
                var method = type.GetMethod(nameof(IProcedure<string>.RunAsync));
                var parmType = method.GetParameters()[0].ParameterType;

                var serialized = serializerValue.Serialize(data.Data);
                var parameter = data.Data != null
                                    ? serializerValue.Deserialize(serialized, parmType)
                                    : Activator.CreateInstance(parmType);

                var item = Activator.CreateInstance(type);

                return (item, method, parameter);
            }

//            if (hasInterface(type, typeof(IComplexProcedure<>)))
//            {
//                var method = type.GetMethod(nameof(IComplexProcedure<string>.RunAsync));
//                var parmType = method.GetParameters()[0].ParameterType;
//
//                var serialized = serializerValue.Serialize(data.Data);
//                var parameter = data.Data != null
//                                    ? serializerValue.Deserialize(serialized, parmType)
//                                    : Activator.CreateInstance(parmType);
//
//                var item = Activator.CreateInstance(type);
//
//                return (item, method, parameter, true);
//            }

            throw new ArgumentOutOfRangeException();
        }
    }
}