﻿namespace Base2art.Bob
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Agent;

    public class ProcedureFinder : IProcedureFinder
    {
        private static readonly StringComparison comp = StringComparison.OrdinalIgnoreCase;
        private readonly Lazy<Tuple<string, string, string, Type>[]> assemblies;

        public ProcedureFinder()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                var name = args.Name.Split(',')[0].Trim();
                var path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), name + ".dll");
                if (File.Exists(path))
                {
                    return Assembly.LoadFile(path);
                }

                return null;
            };

            this.assemblies = new Lazy<Tuple<string, string, string, Type>[]>(Create);
        }

        public Task<Type> Find(string org, string name, string version)
        {
            var part = this.assemblies.Value.FirstOrDefault(x => string.Equals(x.Item1, org, comp)
                                                                 && string.Equals(x.Item2, name, comp)
                                                                 && string.Equals(x.Item3, version, comp));

            return Task.FromResult(part?.Item4);
        }

        private static Tuple<string, string, string, Type>[] Create()
        {
            var profile = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);

            var packages = Path.Combine(profile, ".nuget", "packages");

            var asms = new List<Assembly>();
            if (Directory.Exists(packages))
            {
                var di = new DirectoryInfo(packages);
                asms.AddRange(GetAssemblyFiles(di).Select(x => Assembly.LoadFile(x.FullName)));
            }

            asms.AddRange(GetAssemblies());

            return asms.Select(GetTypesOf)
                       .SelectMany(x => x)
                       .ToArray();
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            var executing = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var di = new DirectoryInfo(executing);
            var items = di.EnumerateFiles("*.dll", SearchOption.AllDirectories);
            return items.Where(x => x.Name.StartsWith("Base2art.Bob.Procedures."))
                        .Select(x => Assembly.LoadFile(x.FullName));
        }

        private static IEnumerable<Tuple<string, string, string, Type>> GetTypesOf(Assembly asm)
        {
            var types = asm.GetExportedTypes();
            foreach (var type in types)
            {
                var attrs = type.GetCustomAttributes<ProcedureAttribute>().ToArray();
                if (attrs.Length == 0)
                {
                    continue;
                }

                var attr = attrs[0];

                yield return Tuple.Create(attr.Org, attr.Name, attr.Version, type);
            }
        }

        private static IEnumerable<FileInfo> GetAssemblyFiles(DirectoryInfo di)
        {
            foreach (var packageDir in di.EnumerateDirectories())
            {
                foreach (var versionDir in packageDir.EnumerateDirectories())
                {
                    var signaller = Path.Combine(versionDir.FullName, ".bob-tasks");
                    if (!File.Exists(signaller))
                    {
                        continue;
                    }

                    var lib = Path.Combine(versionDir.FullName, "lib");
                    if (!Directory.Exists(lib))
                    {
                        continue;
                    }

                    var libDir = new DirectoryInfo(lib);
                    var dirs = libDir.GetDirectories()
                                     .OrderByDescending(x => x.Name)
                                     .Where(x => x.Name.StartsWith("netstandard", comp));

                    var netStandard = dirs.FirstOrDefault();
                    if (netStandard == null)
                    {
                        continue;
                    }

                    var asms = netStandard.EnumerateFiles("*.dll", SearchOption.AllDirectories);
                    foreach (var asm in asms)
                    {
                        yield return asm;
                    }
                }
            }
        }
    }
}