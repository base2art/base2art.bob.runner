﻿namespace Base2art.Bob
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    public class ProgramRunner
    {
        private readonly DirectoryInfo baseDir;

        public ProgramRunner(DirectoryInfo baseDir) => this.baseDir = baseDir;

        public async Task<int> RunAsync(
            TextWriter outputWriter,
            TextWriter errorWriter,
            Options options,
            DirectoryInfo workingDir)
        {
            var rootFileInfos = this.baseDir.GetFiles("*.proc").ToArray();
            var next = this.baseDir.GetDirectories()
                           .FirstOrDefault(x => string.Equals(x.Name, options.Target, StringComparison.OrdinalIgnoreCase));

            if (next != null)
            {
                rootFileInfos = rootFileInfos.Concat(next.GetFiles("*.proc")).ToArray();
            }

            var fileInfos = rootFileInfos.Where(x =>
                                         {
                                             var name = x.Name;
                                             return char.IsDigit(name[0]) && char.IsDigit(name[1]) && char.IsDigit(name[2]) && name[3] == '-';
                                         })
                                         .GroupBy(x => int.Parse(x.Name.Substring(0, 3)))
                                         .OrderBy(x => x.Key);

            var serializer = new InternalSerializer();
            var processRunner = new ProcedureRunner(
                                                    outputWriter,
                                                    errorWriter,
                                                    new ProcedureFinder(),
                                                    serializer);
            var processParser = new ProcedureParser(serializer);

            var procs = fileInfos.Select(procFiles => procFiles.Select(x => processParser.Parse(x))
                                                               .SelectMany(x => x)
                                                               .Select(data => Tuple.Create(procFiles.Key, data)))
                                 .SelectMany(x => x)
                                 .ToList();

            foreach (var proc in procs)
            {
                var level = proc.Item1;
                if (options.MinPhase <= level && level <= options.MaxPhase)
                {
                    var data = proc.Item2;
                    Environment.CurrentDirectory = workingDir.FullName;
                    var result = await processRunner.RunAsync(data, options.WhatIf);
                    if (result != 0)
                    {
                        return result;
                    }
                }
            }

            return 0;
        }
    }
}