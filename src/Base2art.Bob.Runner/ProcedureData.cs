﻿namespace Base2art.Bob
{
    using System.Collections.Generic;

    public class ProcedureData
    {
        public string Id { get; set; }
        public string Org { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }

        public Dictionary<string, object> Data { get; set; }
    }
}