﻿namespace Base2art.Bob
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public class Program
    {
        private static Task<int> Main(string[] args)
        {
            var options = ConsoleParametersParser.Parse(args);

            if (options == null)
            {
                Console.WriteLine("usage: dotnet bob.exe [clean|default|site|0-999] [--target=name] [-whatif]");
                return Task.FromResult(-10);
            }

            var path = Path.Combine(Environment.CurrentDirectory, ".bob");

            if (!Directory.Exists(path))
            {
                Console.WriteLine("usage: dotnet bob.exe [clean|default|site|0-999] [--target=name] [-whatif]");
                Console.WriteLine("no-bob files defined.");
                return Task.FromResult(-10);
            }

            var di = new DirectoryInfo(path);
            var program = new ProgramRunner(di);

            return program.RunAsync(Console.Out, Console.Error, options, new DirectoryInfo(Environment.CurrentDirectory));
        }
    }
}