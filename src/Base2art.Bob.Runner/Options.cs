﻿namespace Base2art.Bob
{
    public class Options
    {
        public bool WhatIf { get; set; }
        public int MaxPhase { get; set; }
        public int MinPhase { get; set; }
        public string Target { get; set; }
    }
}