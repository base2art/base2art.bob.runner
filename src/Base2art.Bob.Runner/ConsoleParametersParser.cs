namespace Base2art.Bob
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ConsoleParametersParser
    {
        public static Options Parse(string[] args)
        {
            var options = new Options();

            var flagable = args.Where(x => x.StartsWith('-')).ToArray();
            var flags = new HashSet<string>(flagable.Where(x => !x.StartsWith("--")), StringComparer.OrdinalIgnoreCase);

            var pairs = flagable.Where(x => x.StartsWith("--")).Select(x => x.Substring(2))
                                .Where(x => x.Contains("="))
                                .Select(x => x.Split("=", 2, StringSplitOptions.RemoveEmptyEntries))
                                .ToDictionary(x => x[0], x => x[1]);

            var others = args.Where(x => !x.StartsWith('-')).ToArray();

            if (!SetPhase(options, others))
            {
                return null;
            }

            SetFlags(options, flags);
            SetPairs(options, pairs);

            return options;
        }

        private static bool SetPhase(Options options, string[] others)
        {
            var phase = "site";
            if (others.Length == 1)
            {
                phase = others[0];
            }

            var phaseValue = GetPhase(phase);
            if (phaseValue == null)
            {
                return false;
            }

            options.MinPhase = phaseValue.Item1;
            options.MaxPhase = phaseValue.Item2;
            return true;
        }

        private static void SetFlags(Options options, HashSet<string> flags)
        {
            if (flags.Contains("-whatif"))
            {
                options.WhatIf = true;
            }
        }

        private static void SetPairs(Options options, Dictionary<string, string> pairs)
        {
            options.Target = pairs.ContainsKey("target") ? pairs["target"] : "--";
        }

        private static Tuple<int, int> GetPhase(string phase)
        {
            phase = (phase ?? string.Empty).ToUpperInvariant();

            switch (phase)
            {
                case "CLEAN":
                    return Tuple.Create(0, 99);
                case "DEFAULT":
                    return Tuple.Create(0, 199);
                case "SITE":
                    return Tuple.Create(0, 299);
                default:
                    return ParsePhase(phase);
            }
        }

        private static Tuple<int, int> ParsePhase(string phase)
        {
            var parts = phase.Split(':');
            if (parts.Length == 1)
            {
                return int.TryParse(parts[0], out var item)
                           ? Tuple.Create(0, item)
                           : null;
            }

            if (parts.Length == 2)
            {
                var min = 0;
                var max = 999;
                if (!string.IsNullOrWhiteSpace(parts[0]) && !int.TryParse(parts[0], out min))
                {
                    return null;
                }

                if (!string.IsNullOrWhiteSpace(parts[1]) && !int.TryParse(parts[1], out max))
                {
                    return null;
                }

                return Tuple.Create(min, max);
            }

            throw new ArgumentOutOfRangeException(nameof(phase), phase, "Out of range...");
        }
    }
}