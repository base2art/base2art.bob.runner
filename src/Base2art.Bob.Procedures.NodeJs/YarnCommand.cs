namespace Base2art.Bob.Procedures.NodeJs
{
    using System.Collections.Generic;

    [Procedure("base2art", "yarn-command", "0.0.0.1")]
    public class YarnCommand : CommandBase<YarnCommandData>
    {
        protected override string ActionTarget(YarnCommandData data) => this.ValueOr(data.Command, this.Command);

        protected override string ExeName() => "yarn";

        protected override string GetExePath(YarnCommandData item) => item.YarnPath;

        protected override IEnumerable<string> NonStandardParams(YarnCommandData data)
            => data.Parameters ?? new string[0];

        private string Command() => "install";
    }
}