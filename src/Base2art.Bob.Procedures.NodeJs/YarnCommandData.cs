namespace Base2art.Bob.Procedures.NodeJs
{
    public class YarnCommandData : CommandBaseData
    {
        public string YarnPath { get; set; }

        public string Command { get; set; }

        public string[] Parameters { get; set; }
    }
}