namespace Base2art.Bob.Procedures.NodeJs
{
    public class NodeCommandData : CommandBaseData
    {
        public string NodePath { get; set; }

        public string Command { get; set; }

        public string[] Parameters { get; set; }
    }
}