namespace Base2art.Bob.Procedures.NodeJs
{
    using System.Collections.Generic;

    [Procedure("base2art", "npm-command", "0.0.0.1")]
    public class NpmCommand : CommandBase<NpmCommandData>
    {
        protected override string ActionTarget(NpmCommandData data) => this.ValueOr(data.Command, this.Command);

        protected override string ExeName() => "npm";

        protected override string GetExePath(NpmCommandData item) => item.NpmPath;

        protected override IEnumerable<string> NonStandardParams(NpmCommandData data)
            => data.Parameters ?? new string[0];

        private string Command() => "install";
    }
}