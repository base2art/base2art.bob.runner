namespace Base2art.Bob.Procedures.NodeJs
{
    public class NpmCommandData : CommandBaseData
    {
        public string NpmPath { get; set; }

        public string Command { get; set; }

        public string[] Parameters { get; set; }
    }
}