namespace Base2art.Bob.Procedures.NodeJs
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Diagnostic;

    public abstract class CommandBase<T> : IProcedure<T>
        where T : CommandBaseData
    {
        public async Task<int> RunAsync(T item, ProcedureParameters defaultParameters)
        {
            var npmPath = await this.ValueOrAsync(this.GetExePath(item), this.CommandPath);
            var targets = this.TargetFiles(item.TargetPath, "package.json");

            var processBuilderFactory = new ProcessBuilderFactory();

            foreach (var target in targets)
            {
                var result = await processBuilderFactory.Create()
                                                        .WithExecutable(npmPath)
                                                        .WithArguments(new[] {this.ActionTarget(item)}
                                                                       .Concat(this.NonStandardParams(item)).ToArray())
                                                        .WithWorkingDirectory(target.Directory.FullName)
                                                        .WithOutputWriter(defaultParameters.OutputWriter)
                                                        .WithErrorWriter(defaultParameters.ErrorWriter)
                                                        .Execute();

                if (result != 0)
                {
                    return result;
                }
            }

            return 0;
        }

        protected abstract string GetExePath(T item);

        protected virtual IEnumerable<string> NonStandardParams(T item) => new string[] { };

        protected abstract string ActionTarget(T item);

        private async Task<string> CommandPath()
        {
            var name = this.ExeName();
            var result = await Environments.FindFileInPath(name, $"{name}.cmd", $"{name}.exe");
            return result == null ? name : result.FullName;
        }

        protected abstract string ExeName();
    }
}