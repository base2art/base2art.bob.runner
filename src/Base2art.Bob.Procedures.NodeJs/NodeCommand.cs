namespace Base2art.Bob.Procedures.NodeJs
{
    using System.Collections.Generic;

    [Procedure("base2art", "node-command", "0.0.0.1")]
    public class NodeCommand : CommandBase<NodeCommandData>
    {
        protected override string ActionTarget(NodeCommandData data) => this.ValueOr(data.Command, this.Command);

        protected override string ExeName() => "node";

        protected override string GetExePath(NodeCommandData item) => item.NodePath;

        protected override IEnumerable<string> NonStandardParams(NodeCommandData data)
            => data.Parameters ?? new string[0];

        private string Command() => "run";
    }
}