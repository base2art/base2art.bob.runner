﻿namespace Base2art.Bob.Procedures.Zip
{
    public class ZipExpandData
    {
        public string SourceFile { get; set; }
        public string DestinationDirectory { get; set; }
    }
}