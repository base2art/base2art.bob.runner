﻿namespace Base2art.Bob.Procedures.Zip
{
    using System.IO.Compression;
    using System.Threading.Tasks;

    [Procedure("base2art", "zip-expand", "0.0.0.1")]
    public class ZipExpand : IProcedure<ZipExpandData>
    {
        public Task<int> RunAsync(ZipExpandData item, ProcedureParameters defaultParameters)
        {
            ZipFile.ExtractToDirectory(
                                       item.SourceFile,
                                       item.DestinationDirectory);

            return Task.FromResult(0);
        }
    }
}