namespace Base2art.Bob.Procedures.FileSystem
{
    public class CopyItemProcedureData
    {
        public string Input { get; set; }
        public string Output { get; set; }
    }
}