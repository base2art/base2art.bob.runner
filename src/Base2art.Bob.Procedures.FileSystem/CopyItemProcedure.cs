namespace Base2art.Bob.Procedures.FileSystem
{
    using System.IO;
    using System.Threading.Tasks;

    [Procedure("base2art", "filesystem-copy-entry", "0.0.0.1")]
    public class CopyItemProcedure : IProcedure<CopyItemProcedureData>
    {
        public Task<int> RunAsync(CopyItemProcedureData parameters, ProcedureParameters defaultParameters)
        {
            var input = parameters.Input;
            var output = parameters.Output;
            if (File.Exists(input))
            {
                if (Directory.Exists(output))
                {
                    Directory.CreateDirectory(output);
                    output = Path.Combine(output, Path.GetFileName(input));
                }

                File.Copy(input, output);
            }
            else if (Directory.Exists(input))
            {
                DirectoryCopy(input, output, true);
            }
            else
            {
                throw new DirectoryNotFoundException($"Source file/directory does not exist or could not be found: {input}");
            }

            return Task.FromResult(0);
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            var dir = new DirectoryInfo(sourceDirName);
            var dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            var files = dir.GetFiles();
            foreach (var file in files)
            {
                var temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (var subdir in dirs)
                {
                    var temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
    }
}