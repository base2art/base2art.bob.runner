﻿namespace Base2art.Bob.Procedures.FileSystem
{
    using System.IO;
    using System.Threading.Tasks;

    [Procedure("base2art", "filesystem-delete-entry", "0.0.0.1")]
    public class DeleteFileSystemEntryProcedure : IProcedure<DeleteFileSystemEntryData>
    {
        public Task<int> RunAsync(DeleteFileSystemEntryData item, ProcedureParameters defaultParameters)
        {
            if (File.Exists(item.Path))
            {
                File.Delete(item.Path);
                return Task.FromResult(0);
            }

            if (Directory.Exists(item.Path))
            {
                Directory.Delete(item.Path, true);
                return Task.FromResult(0);
            }

            return Task.FromResult(0);
        }
    }
}