﻿namespace Base2art.Bob.Procedures.FileSystem
{
    public class DeleteFileSystemEntryData
    {
        public string Path { get; set; }
    }
}