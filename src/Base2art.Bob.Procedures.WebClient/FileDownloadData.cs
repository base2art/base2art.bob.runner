﻿namespace Base2art.Bob.Procedures.WebClient
{
    using System.Collections.Generic;

    public class FileDownloadData
    {
        public string Url { get; set; }
        public Dictionary<string, string> Headers { get; set; }
        public string OutputFile { get; set; }
        public bool OverwriteFile { get; set; }
    }
}