﻿namespace Base2art.Bob.Procedures.WebClient
{
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Threading.Tasks;

    [Procedure("base2art", "webclient-download", "0.0.0.1")]
    public class FileDownload : IProcedure<FileDownloadData>
    {
        public async Task<int> RunAsync(FileDownloadData item, ProcedureParameters defaultParameters)
        {
            var url = this.FileOrValue(item.Url, skipDownload: true);

            if (string.IsNullOrWhiteSpace(url))
            {
                return 0;
            }

            var headers = item.Headers ?? new Dictionary<string, string>();

            if (!item.OverwriteFile && File.Exists(item.OutputFile))
            {
                return 0;
            }

            using (var myWebClient = new WebClient())
            {
                foreach (var header in headers)
                {
                    myWebClient.Headers.Add(header.Key, header.Value);
                }

                var dir = Path.GetDirectoryName(item.OutputFile);
                if (!string.IsNullOrWhiteSpace(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                await myWebClient.DownloadFileTaskAsync(url, item.OutputFile);
            }

            return 0;
        }
    }
}