namespace Base2art.Bob.Procedures.Packaging
{
    public class PackageWebProjectData
    {
        public string OutputDirectory { get; set; }
        public string OutputFile { get; set; }
        public string WebRootFileIndicator { get; set; }
        public string OutputExtension { get; set; }

        // FILE OR VALUE
        public string DefaultBranchName { get; set; }

        // FILE OR VALUE
        public string BranchName { get; set; }

        // FILE OR VALUE
        public string Version { get; set; }

        public string WebRoot { get; set; }
    }
}