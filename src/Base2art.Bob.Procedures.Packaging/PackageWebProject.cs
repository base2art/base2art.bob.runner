namespace Base2art.Bob.Procedures.Packaging
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Threading.Tasks;

    [Procedure("base2art", "web-package-zip", "0.0.0.1")]
    public class PackageWebProject : IProcedure<PackageWebProjectData>
    {
        public Task<int> RunAsync(PackageWebProjectData item, ProcedureParameters defaultParameters)
        {
            var outputFile = this.OutputFile(item.OutputFile);
            var outputExtension = this.OutputExtension(item.OutputExtension);
            var outputDirectory = this.OutputDirectory(item.OutputDirectory);

            var branchName = this.BranchName(item.BranchName);
            var version = this.Version(item.Version);

            Directory.CreateDirectory(outputDirectory);

            var webDirectories = this.TargetDirectories(item.WebRoot,
                                                        x => Procedures.FilesByExtension(x, this.WebRootFileIndicator(item.WebRootFileIndicator))
                                                                       .Select(y => y.Directory).ToArray());

            for (var index = 0; index < webDirectories.Length; index++)
            {
                var path = webDirectories[index];
                var output = Directory.CreateDirectory(Path.Combine(".deploy", ".web" + index));
                CopyDir.Copy(path.FullName, output.FullName);

                var compressionLevel = CompressionLevel.Optimal;

                if (item.DefaultBranchName != null && string.Equals(item.DefaultBranchName, branchName, StringComparison.OrdinalIgnoreCase))
                {
                    branchName = "";
                }

                var realName = RealName(outputFile, outputExtension, branchName, version);

                ZipFile.CreateFromDirectory(output.FullName, Path.Combine(outputDirectory, realName), compressionLevel, false);
            }

            return Task.FromResult(0);
        }

        private static string RealName(string outputFile, string outputExtension, string branchName, string version)
        {
            string RealNameInner(string outputFileInner, string outputExtensionInner, string branchNameInner, string versionInner)
            {
                if (outputFileInner.EndsWith(outputExtensionInner, StringComparison.OrdinalIgnoreCase))
                {
                    return outputFileInner;
                }

                if (string.IsNullOrWhiteSpace(branchNameInner) && string.IsNullOrWhiteSpace(versionInner))
                {
                    return $"{outputFileInner}{outputExtensionInner}";
                }

                if (string.IsNullOrWhiteSpace(branchNameInner))
                {
                    return $"{outputFileInner}.{versionInner}{outputExtensionInner}";
                }

                if (string.IsNullOrWhiteSpace(versionInner))
                {
                    return $"{outputFileInner}-{branchNameInner}{outputExtensionInner}";
                }

                return $"{outputFileInner}-{branchNameInner}.{versionInner}{outputExtensionInner}";
            }

            var item = RealNameInner(outputFile, outputExtension, branchName, version);
            HashSet<char> invalid = new HashSet<char>(Path.GetInvalidFileNameChars());

            return new string(item.Select(x => invalid.Contains(x) ? '-' : x).ToArray());
        }

        private string Version(string version) => this.FileOrValue(version)?.Trim();

        private string BranchName(string branchName) => this.FileOrValue(branchName)?.Trim();

        private string WebRootFileIndicator(string webRootFileIndicator) =>
            string.IsNullOrWhiteSpace(webRootFileIndicator) ? "web.config" : webRootFileIndicator;

        private string OutputDirectory(string outputDirectory) => string.IsNullOrWhiteSpace(outputDirectory) ? ".deploy" : outputDirectory;

        private string OutputFile(string outputFile) => string.IsNullOrWhiteSpace(outputFile) ? "web.zip" : outputFile;

        private string OutputExtension(string outputExtension) => string.IsNullOrWhiteSpace(outputExtension) ? ".zip" : outputExtension;
    }
}