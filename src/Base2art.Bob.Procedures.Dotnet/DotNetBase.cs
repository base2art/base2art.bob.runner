namespace Base2art.Bob.Procedures.Dotnet
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Diagnostic;
    using DotNet;

    public abstract class DotNetBase<T> : IProcedure<T>
        where T : DotNetBaseData
    {
        public async Task<int> RunAsync(T item, ProcedureParameters defaultParameters)
        {
            var dotNetPath = await this.ValueOrAsync(item.DotNetPath, this.DotNetPath);
            var targets = this.TargetFiles(item.TargetPath, this.ExtensionFilter());
            targets = targets.Where(this.IncludeTarget).ToArray();

            var configuration = this.ValueOr(item.Configuration, this.Configuration);

            var processBuilderFactory = new ProcessBuilderFactory();

            foreach (var target in targets)
            {
                var result = await processBuilderFactory.Execute(
                                                                 defaultParameters,
                                                                 dotNetPath,
                                                                 new[]
                                                                 {
                                                                     this.ActionTarget(),
                                                                     target.FullName,
                                                                     "--configuration",
                                                                     configuration,
                                                                     "-v",
                                                                     "q"
                                                                 }.Concat(this.NonStandardParams(item)).ToArray());

                if (result != 0)
                {
                    return result;
                }

                var result1 = await this.PostProcess(item, target);
                if (result1 != 0)
                {
                    return result1;
                }
            }

            return 0;
        }

        protected virtual Task<int> PostProcess(T item, FileInfo target) => Task.FromResult(0);

        protected virtual IEnumerable<string> NonStandardParams(T parameters) => new string[] { };

        protected virtual bool IncludeTarget(FileInfo path) => true;

        protected virtual string ExtensionFilter() => "*.sln";

        protected abstract string ActionTarget();

        private string Configuration() => "Release";

        private async Task<string> DotNetPath()
        {
            var result = await Environments.FindFileInPath("dotnet", "dotnet.exe");
            if (result == null)
            {
                return "dotnet";
            }

            return result.FullName;
        }
    }
}