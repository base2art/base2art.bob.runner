namespace Base2art.Bob.Procedures.DotNet
{
    public class DotNetBaseData
    {
        public string DotNetPath { get; set; }
        public string TargetPath { get; set; }
        public string Configuration { get; set; }
    }
}