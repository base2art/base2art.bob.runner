namespace Base2art.Bob.Procedures.DotNet
{
    using Dotnet;

    [Procedure("base2art", "DotNet-clean", "0.0.0.1")]
    public class DotNetClean : DotNetBase<DotNetCleanData>
    {
        protected override string ActionTarget() => "clean";
    }
}