namespace Base2art.Bob.Procedures.DotNet
{
    using Dotnet;

    [Procedure("base2art", "DotNet-publish", "0.0.0.1")]
    public class DotNetPublish : DotNetBase<DotNetPublishData>
    {
        protected override string ActionTarget() => "publish";
    }
}