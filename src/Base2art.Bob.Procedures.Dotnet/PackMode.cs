namespace Base2art.Bob.Procedures.DotNet
{
    public enum PackMode
    {
        NoSymbolsOrSource,

        SymbolsAndSourceOnly,

        SymbolsOnly,
        SourceOnly
    }
}