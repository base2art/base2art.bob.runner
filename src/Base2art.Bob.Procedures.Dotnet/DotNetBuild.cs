namespace Base2art.Bob.Procedures.DotNet
{
    using Dotnet;

    [Procedure("base2art", "DotNet-build", "0.0.0.1")]
    public class DotNetBuild : DotNetBase<DotNetBuildData>
    {
        protected override string ActionTarget() => "build";
    }
}