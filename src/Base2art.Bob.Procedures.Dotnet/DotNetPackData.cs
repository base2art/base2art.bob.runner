namespace Base2art.Bob.Procedures.DotNet
{
    public class DotNetPackData : DotNetBaseData
    {
        public string Version { get; set; }

        public PackMode PackMode { get; set; }
    }
}