namespace Base2art.Bob.Procedures.DotNet
{
    using System;
    using System.IO;
    using Dotnet;

    [Procedure("base2art", "DotNet-test", "0.0.0.1")]
    public class DotNetTest : DotNetBase<DotNetTestData>
    {
        protected override string ActionTarget() => "test";

        protected override string ExtensionFilter() => "*.csproj";

        protected override bool IncludeTarget(FileInfo path)
        {
            try
            {
                return path.Exists &&
                       File.ReadAllText(path.FullName).ToUpperInvariant().Contains("Microsoft.NET.Test.Sdk".ToUpperInvariant());
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}