namespace Base2art.Bob.Procedures.DotNet
{
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Dotnet;

    [Procedure("base2art", "DotNet-pack", "0.0.0.1")]
    public class DotNetPack : DotNetBase<DotNetPackData>
    {
        protected override string ActionTarget() => "pack";

        protected override IEnumerable<string> NonStandardParams(DotNetPackData data)
        {
            var parms = new List<string>();

            if (data.PackMode == PackMode.SymbolsOnly || data.PackMode == PackMode.SymbolsAndSourceOnly)
            {
                parms.Add("--include-symbols");
            }

            if (data.PackMode == PackMode.SourceOnly || data.PackMode == PackMode.SymbolsAndSourceOnly)
            {
                parms.Add("--include-source");
            }

            var version = this.FileOrValue(data.Version);
            if (!string.IsNullOrWhiteSpace(version))
            {
                parms.Add($"/p:PackageVersion={version}");
            }

            return parms.ToArray();
        }

        protected override Task<int> PostProcess(DotNetPackData item, FileInfo target)
        {
            if (item.PackMode == PackMode.NoSymbolsOrSource)
            {
                return Task.FromResult(0);
            }

            var projects = this.TargetFiles(null, "*.symbols.nupkg");

            foreach (var project in projects)
            {
                var dest = project.FullName.Replace(".symbols.nupkg", ".nupkg");
                File.Delete(dest);
                project.MoveTo(dest);
            }

            return Task.FromResult(0);
        }
    }
}